import styled from "styled-components";

export const CartWrapper = styled.div`
  text-align: center;
`;

export const CartMessage = styled.p`
  color: "#224E69";
  font-size: 24px;
  padding-top: 100px;
`;
export const CardsList = styled.ul`
  display: flex;
  flex-wrap: wrap;
`;
