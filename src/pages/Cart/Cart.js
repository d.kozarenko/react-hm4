import React from "react";
import { CartMessage, CardsList, CartWrapper } from "./Cart-styles";
import Product from "../../components/Product/Product";
import { connect } from "react-redux";

const Cart = ({ cart, goods }) => {
  const cartArr = [];

  goods.forEach((g) => {
    if (cart.includes(g.id)) {
      cartArr.push(g);
    }
  });

  const cartList = cartArr.map((c) => (
    <Product id={c.id} key={c.id} product={c} delBtn />
  ));

  return (
    <CartWrapper>
      {cart.length === 0 && <CartMessage>В корзине пока пусто</CartMessage>}
      {cart.length !== 0 && <CardsList>{cartList}</CardsList>}
    </CartWrapper>
  );
};

const mapStateToProps = (state) => {
  return {
    goods: state.goods.data,
    cart: state.cart.storage,
  };
};

export default connect(mapStateToProps)(Cart);
