import React from "react";
import Svg from "../Svg/Svg.js";
import Button from "../Button/Button.js";
import { Link } from "react-router-dom";
import { modalOperations } from "../../store/modal/index.js";
import { favoritesOperations } from "../../store/favorites/index.js";
import { connect } from "react-redux";
import {
  ProductItem,
  Img,
  DefaultLink,
  TextLink,
  Color,
  Price,
  PurchaseWrapper,
  Header,
  Body,
  Footer,
  linkStyles,
} from "./Product-styles.js";

const Product = ({
  product,
  id,
  cart,
  favorites,
  delBtn,
  setFavorites,
  saveModalSettings,
}) => {
  const fill = favorites.find((el) => el === id) ? "#ffa500" : "#fff";

  const addToFavorites = (id) => {
    const sameValue = favorites.find((el) => el === id);

    const favoriteValue = sameValue
      ? favorites.filter((el) => el !== sameValue)
      : [...favorites, Number(id)];

    setFavorites(favoriteValue);

    localStorage.setItem("Favorites", JSON.stringify(favoriteValue));
  };

  const purchaseModalContent = [
    false,
    "Подтверждение",
    "purchase_modal",
    "#fff",
    "#6666ff",
    "Вы хотите добавить товар в корзину?",
  ];

  const deleteModalContent = [
    false,
    "Вы уверены?",
    "delete_modal",
    "#fff",
    "#B20000",
    "Вы хотите удалить товар в из корзины?",
  ];

  return (
    <ProductItem id={id}>
      <Header>
        {!delBtn && (
          <Svg
            fill={fill}
            onClick={() => {
              addToFavorites(id);
            }}
          />
        )}
        {delBtn && (
          <Button
            bgColor={"transparent"}
            color={"black"}
            margin={"0 4px 0 0"}
            text={"x"}
            fontSize={"20px"}
            handleClick={() => {
              saveModalSettings(deleteModalContent);
              document.getElementById(`${id}`).classList.add("activeCard");
            }}
          ></Button>
        )}
      </Header>
      <Body>
        <DefaultLink>
          <Img src={product.src}></Img>
        </DefaultLink>
      </Body>
      <Footer>
        <TextLink>{product.name}</TextLink>
        <Color>Цвет: {product.color}</Color>
        <PurchaseWrapper>
          <Price>{product.price}$</Price>
          {/* когда товара нет в локалсторедже (страница товаров) */}
          {!cart.find((el) => el === id) && (
            <Button
              padding={"10px"}
              bgColor={"#ADD8E6"}
              margin={"0 5px 0 5px"}
              text={"Купить"}
              className={`purchaseBtn`}
              handleClick={() => {
                saveModalSettings(purchaseModalContent);
                document.getElementById(`${id}`).classList.add("activeCard");
              }}
            />
          )}
          {/* когда товар в локалсторедже, показывается надпись "в корзине" для перехода в корзину (страница товаров) */}
          {cart.find((el) => el === id) && !delBtn && (
            <Link to="/cart" style={linkStyles}>
              В корзине
            </Link>
          )}
          {/* когда товар в локалсторедже (страница корзины) */}
          {cart.find((el) => el === id) && delBtn && (
            <Button
              padding={"10px"}
              bgColor={"#ADD8E6"}
              margin={"0 5px 0 5px"}
              text={"Удалить"}
              className={`purchaseBtn`}
              handleClick={() => {
                saveModalSettings(deleteModalContent);
                document.getElementById(`${id}`).classList.add("activeCard");
              }}
            />
          )}
        </PurchaseWrapper>
      </Footer>
    </ProductItem>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.cart.storage,
    favorites: state.favorites.storage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveModalSettings: (settings) =>
      dispatch(modalOperations.saveModalSettings(settings)),
    setFavorites: (favoritesValue) =>
      dispatch(favoritesOperations.setFavorites(favoritesValue)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
