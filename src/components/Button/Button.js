import React from "react";
import { StyledButton } from "./Button-styles.js";

const Button = ({ text, handleClick, className, id, ...rest }) => {
  return (
    <StyledButton className={className} id={id} {...rest} onClick={handleClick}>
      {text}
    </StyledButton>
  );
};

export default Button;
