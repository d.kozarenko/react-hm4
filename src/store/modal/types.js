const SHOW_MODAL = "react-hm4/modal/SHOW_MODAL";
const CUSTOMIZE_MODAL = "react-hm4/modal/CUSTOMIZE_MODAL";

const typesObj = { SHOW_MODAL, CUSTOMIZE_MODAL };

export default typesObj;
