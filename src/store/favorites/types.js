const UPDATE_FAVORITES = "react-hm4/favorites/UPDATE_FAVORITES";

const typesObj = { UPDATE_FAVORITES };

export default typesObj;
