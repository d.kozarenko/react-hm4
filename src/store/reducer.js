import { combineReducers } from "redux";

import goods from "./goods";
import cart from "./cart";
import favorites from "./favorites";
import modal from "./modal";

const reducer = combineReducers({
  goods,
  cart,
  favorites,
  modal,
});

export default reducer;
