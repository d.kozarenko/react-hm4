const IS_LOADING = "react-hm4/goods/IS_LOADING";
const SAVE_GOODS = "react-hm4/goods/SAVE_GOODS";

const typesObj = { IS_LOADING, SAVE_GOODS };

export default typesObj;
