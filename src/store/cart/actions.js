import types from "./types";

const updateCart = (cartValue) => ({
  type: types.UPDATE_CART,
  payload: cartValue,
});

const actionsObj = { updateCart };

export default actionsObj;
