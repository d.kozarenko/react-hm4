const UPDATE_CART = "react-hm4/cart/UPDATE_CART";

const typesObj = { UPDATE_CART };
// объекты для экспорта создаются чтобы убрать ворнинг из консоли: "Assign object to a variable before exporting as module default import/no-anonymous-default-export"

export default typesObj;
