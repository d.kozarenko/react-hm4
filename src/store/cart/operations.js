import actions from "./actions";

const setCart = (cartValue) => (dispatch, getState) => {
  dispatch(actions.updateCart(cartValue));
};

const operationsObj = { setCart };

export default operationsObj;
