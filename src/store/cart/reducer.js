import types from "./types";

const initialState = {
  storage: JSON.parse(localStorage.getItem("Cart")) || [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_CART: {
      return {
        ...state,
        storage: action.payload,
      };
    }
    default:
      return state;
  }
};

export default reducer;
